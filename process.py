from Bio import SeqIO
import itertools
import os
import math
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
from Bio.Emboss.Applications import WaterCommandline
#returns the first non-False value
def retOnFirst(func, iterable):
	for x in iterable:
		res = func(x)
		if res is not False:
			yield res
			break
	yield None

#The following function aligns two sequences, and returns the two aligned ones as a tuple. We use EMBOSS water for this!
def alignSequences(seq_one, seq_two):
	#write the sequences to FASTA files
	handleOne = open('first.fasta', 'w')
	
	handleOne.write('>first \n' + seq_one)
	handleOne.close()
	handleTwo = open('second.fasta', 'w')
	handleTwo.write('>second \n' + seq_two)
	handleTwo.close()
	command = 'water -outfile=temp_water.txt -asequence first.fasta -bsequence second.fasta -gapopen=10 -gapextend=0.5'
	os.system(command)
	results_handle = open('temp_water.txt', 'r')
	results = results_handle.read()
	results_handle.close()
	#now parse the output
	lines = results.split('\n')
	#now put into groups. We want the 'first' and 'second' groups
	first = list()
	second = list()
	for x in lines:

		parts = x.split(' ')


		if parts[0] == 'first':
			first.append(filter(lambda x: 'A' in  x or 'G' in x or 'N' in x or 'T' in x or 'C' in x or '-' in x, parts[1::]))
		elif parts[0] == 'second':
			second.append(filter(lambda x: 'A' in  x or 'G' in x or 'N' in x or 'T' in x or 'C' in x or '-' in x, parts[1::]))




	return (''.join(itertools.chain(*first)), ''.join(itertools.chain(*second)))
def distance(seq_one, seq_two):
	first_aligned, second_aligned = alignSequences(seq_one, seq_two)
	score =  1.0*len(filter(lambda (x,y): x is not y, zip(first_aligned, second_aligned)))/len(seq_one)
	print 'identical'
	print 1.0-score
	print 'non-identical'
	print score
	return (-.75 * math.log(1.0 - 1.25*score))
"""
This function is deprecated, and no longer needed. I'm just keeping it here because it's a good use of functional programming techniques. The function is the remnnants of a script that searched Entrez. 
"""
def get_genes(organism):
	gene_sort = lambda y: y['gene']
	sorted_organism = sorted(organism, key=gene_sort)
	gene_groups = dict((k, list(val)) for k, val in itertools.groupby(sorted_organism, gene_sort))



	return {k:v[0] for k,v in gene_groups.items()}
	return dict(map(lambda x: (x[0], consensus(map(lambda y: y['sequence'], x[1]))), gene_groups.items()))

"""
Ditto for this function.
"""

#genome records are from SeqIO.parse, from the genome's FASTA file.
def search_c_genome(gene_name):
	genome_records = c_genome
	z = lambda x: [x] + z(x[:-1]) if len(x) > 1 else [x]
	names_to_try = z(gene_name)
	genes_to_search = dict(map(lambda x: (x.description.split()[1], str(x.seq)), genome_records))

	#print genes_to_search
	#longest name.
	nts = genes_to_search.keys()


	genome_genes = list(itertools.takewhile(lambda x: x in nts, names_to_try))


	if len(genome_genes) == 0:
		print "that didn't work. Resolve manually: " + gene_name
	else:
		return str(genes_to_search[genome_genes[0]])




"""start the new script! """
e_handle = open('e.fasta', 'r')
e_records = SeqIO.parse(e_handle, 'fasta')

c_handle = open('cerevisiae.fasta', 'r')
c_records = SeqIO.to_dict(SeqIO.parse(c_handle, 'fasta'))

p_handle =  open('p.fasta', 'r')
p_records = SeqIO.to_dict(SeqIO.parse(p_handle, 'fasta'))



c_distances = list()
e_distances = list()

labels = list()
#For genes that are in only 2 of the organisms, we want to draw a line to indicate relatedness.
e_lines = list()
c_lines = list()

for x in e_records:
	name = str(x.id)
	if name in c_records.keys() and name in p_records.keys():
		c_seq = str(c_records[name].seq)
		p_seq = str(p_records[name].seq)
		e_seq = str(x.seq)
		print 'gene:'
		print name
		print 'p-c'
		c_dist = distance(p_seq, c_seq)
		print 'p-e'
		e_dist = distance(p_seq, e_seq)


		c_distances.append(c_dist)
		e_distances.append(e_dist)

		labels.append(name)
e_handle.close()
e_handle = open('e.fasta', 'r')
e_records = SeqIO.to_dict(SeqIO.parse(e_handle, 'fasta'))



for x in p_records.keys():


	p_seq = str(p_records[x].seq)
	if x in e_records.keys() and x not in c_records.keys():
		print 'shared between p and e'
		print x
		e_seq = str(e_records[x].seq)
		dist = distance(p_seq, e_seq)
		e_lines.append(dist)
	elif x in c_records.keys() and x not in e_records.keys():
		print 'shared between p and c'
		print x
		c_seq = str(c_records[x].seq)
		dist = distance(p_seq, c_seq)
		c_lines.append(dist)
	else:
		print "doesn't work"
		print x
e_handle.close()
c_handle.close()
p_handle.close()

plt.scatter(c_distances, e_distances)
plt.xlabel('distance between pastorianus and cerevisiae')
plt.ylabel('distance between pastorianus and eubayanus')
plt.suptitle('distance plot')
for label, x, y in zip(labels, c_distances, e_distances):
    plt.annotate(label, xy = (x, y), xytext = (-5, 5), textcoords = 'offset points', ha = 'right', va = 'bottom')

print 'e lines'
print e_lines
for x in e_lines:
	#draw horizontal line
	plt.axhline(x)
"""
for x in n_lines:
	plt.axvline(x, color='g')
"""
for x in c_lines:
	plt.axvline(x)
c_average = (sum(c_lines) + sum(c_distances))/(1.0*(len(c_lines) + len(c_distances)))
e_average = (sum(e_lines) + sum(e_distances))/(1.0*(len(e_distances) + len(e_lines)))

plt.axvline(c_average, color='r')
plt.axhline(e_average, color='r')
#label where the average, red lines intersect
label = '(' + str(c_average.round(3)) + ', ' + str(e_average.round(3))+ ')'
plt.annotate(label, xy=(c_average, e_average),  xycoords='data', xytext=(10, 10), textcoords='offset points', arrowprops=dict(arrowstyle="->"))

ylim(0)
xlim(0)
plt.show()
plt.scatter(c_distances, e_distances)
plt.xlabel('distance between pastorianus and cerevisiae')
plt.ylabel('distance between pastorianus and eubayanus')
plt.suptitle('distance plot')
for label, x, y in zip(labels, c_distances, e_distances):
    plt.annotate(label, xy = (x, y), xytext = (-5, 5), textcoords = 'offset points', ha = 'right', va = 'bottom')
plt.show()
"""end the new script! 
print 'genes'
print genes.items()
scores = filter(lambda x: x is not None, map(get_gene_score, genes.items()))
print 'scores'
print scores
plt.hist(scores)
plt.figure()
"""
