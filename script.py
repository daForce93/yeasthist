from Bio import SeqIO, Entrez, pairwise2
import itertools
import pickle
from Bio.SubsMat import MatrixInfo
#let's first grab all of the records.
#Add more later!
#786711
ids = map(lambda x: 'JF' + str(x), range(786614, 786711))
print 'ids'
print ids

Entrez.email = 'jordan.force@uconn.edu'
#This aligns the sequences, then uses the Jukes-Cantor method to compute a score


def get_record(accession_id):
	try:
		#this function returns the gene name, organism, strain and DNA sequence
		handle = Entrez.efetch(db='nucleotide', id=accession_id, rettype='gb', retmode='text')
		record = SeqIO.read(handle, 'genbank')
		handle.close()
		features = {x.type:x for x in record.features}
		sequence = str(record.seq)[int(features['gene'].location.start):int(features['gene'].location.end)]
		return {'organism':features['source'].qualifiers['organism'][0], 'strain':features['source'].qualifiers['strain'][0], 'gene':features['gene'].qualifiers['gene'][0], 'sequence':sequence, 'id':accession_id}
	except Exception, e:
		return None
records = filter(None, map(get_record, ids))
print 'records'
print records
#sort records according to organism name
sorted_records = sorted(records, key = lambda x: x['organism'])
#it's grouped by organism
groups = dict((k, list(val)) for k, val in itertools.groupby(sorted_records, lambda x: x['organism']))
f = open('groups.pickle', 'w')
pickle.dump(groups, f)
f.close()
"""
#now, go through, and group each organism by gene
def get_gene_groups(organism):
	gene_sort = lambda y: y['gene']
	sorted_organism = sorted(organism, key=gene_sort)
	return dict((key, list(val)) for k, val in itertools.groupby(sorted_organism, gene_sort)))
gene_groups = dict(map(get_gene_groups, groups))
print 'gene groups'
print gene_groups
#for each gene, first, grab the gene in S. c (from Entrez) and Lager Yeast. , and run alignment using clustalo, and get the distance, between the lager yeast gene and S. e and S. c. 
"""
